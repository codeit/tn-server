function build_virtual_value(values) {
    if (values && values.length > 0) {
        return values.join("^");
    }
}
function roundNumber(number) {
    if (isNaN(number)) return number;
    return Math.round(number * 100) / 100
}

export { build_virtual_value, roundNumber }