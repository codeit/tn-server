import log4js from "log4js";
import config from "../../config/log4js.json" with { type: "json" };
log4js.configure(config);
const mode = Deno.env.get("MODE") || "dev";
const logger = log4js.getLogger(mode === "dev" ? "dev" : "prod");
logger.level = "info";
export default logger;