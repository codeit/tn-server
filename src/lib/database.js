import { Sequelize } from 'sequelize';
import config from '../../config/config.json' with { type: "json" };
import logger from './log4js.js';
const sequelize = new Sequelize({
    dialect: 'mariadb',
    dialectOptions: {
        ...config.database
    },
    pool: {
        max: 20,
        min: 1,
        acquire: 30000,
        idle: 10000
    },
    logging: false,//msg => logger.debug(msg),
    logQueryParameters: true,


});
const buildOrder = (order) => {
    logger.debug(`order : ${JSON.stringify(order)}`);
    if (!order) return null;
    if (typeof (order) === "object") {
        if (order.length === 0) return null;
        if (order[0].length === 0) return null;
        return order;
    }
    try {

        const ord = JSON.parse(order);
        if (ord.length === 0) return null;
        let empty = 0;
        for (const od of ord) {
            if (od.length === 0) {
                empty += 1;
            }
        }
        if (empty === ord.length) return null;
        logger.debug(ord);
        return ord;
    } catch (error) {
        logger.debug(error);
        return null;
    }
}
const buildWhere = (where) => {
    logger.debug(typeof (where), where);
    if (!where) return null;
    if (typeof (where) === "object") return where;
    try {
        const wh = JSON.parse(where);
        if (!wh && Object.keys(wh).length === 0) return null;
        return wh;
    } catch (error) {
        logger.error(error);
        return null;
    }

}
export { sequelize, buildOrder, buildWhere };