type LeaderResult = {
    user_name: string;
    amount: number;
    政治能力: number;
    政治表现: number;
    创新精神: number;
    创新成果: number;
    专业素养: number;
    领导能力: number;
    担当作为: number;
    履职绩效: number;
    一岗双责: number;
    廉洁从业: number
}

type LeaderGroupResult = {
    user_name: string,
    amount: number,
    政治担当: number,
    社会责任: number,
    经营效益: number,
    管理效能: number,
    风险管控: number,
    科技创新: number,
    人才强企: number,
    深化改革: number,
    选人用人: number,
    基层党建: number,
    党风廉政: number,
    团结协作: number,
    联系群众: number,
}

type ExtDirectorResult = {
    user_name: string,
    amount: number,
    忠实勤勉: number,
    严于律己: number,
    科学决策: number,
    监督问效: number,
    建言献策: number
}
type SheetData = {
    title?: string,
    groupName?: string,
    name?: string,
    data: Array<Record<string,?>>,
    data2?: Array<Record<string,?>>
}

type MergeRange = {
    s: { c: number, r: number },
    e: { c: number, r: number }
}
type SheetMeta = {
    merges: Array<MergeRange>,
    data: Array<Array<?>>,
    name: string
}
type RuleTop = {
    [key: string | String]: { items: Array<String | string>, rate?: number, child_rate: number[] }
}