import { BaseDataMapper } from "./base-mapper.js";
import bcrypt from 'bcrypt';
//import * as bcrypt from "https://deno.land/x/bcrypt/mod.ts";
export class AdminUserMapper extends BaseDataMapper {
    constructor() {
        super("AdminUser");
    }
    async saveOrUpdate({ values }) {
        if (values.store_pass) {
            const hasedPass = await bcrypt.hash(values.store_pass, 10);
            values.store_pass = hasedPass;
        }
        return await super.saveOrUpdate({ values });

    }
}