import { QueryTypes } from "sequelize";
import { sequelize } from "../lib/database.js";
import { BaseDataMapper } from "./base-mapper.js";

export class RuSecondRuleMapper extends BaseDataMapper {

    constructor() {
        super("RuSecondRule");
    }

    async findTopRule(id: string) {
        const sql = `select rtr.id,rtr.name from ru_second_rule rsr 
                        inner join ru_top_rule rtr rtr.id = rsr.top_id 
                        where rsr.id=?
                    limit 1`
        const results = await sequelize.query(sql,
            {
                bind: [id],
                type: QueryTypes.SELECT
            }
        );
        return results[0];
    }
}