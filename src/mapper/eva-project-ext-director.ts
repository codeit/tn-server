import { BaseDataMapper } from "./base-mapper.js";
export class EvaProjectExtDirectorMapper extends BaseDataMapper {
    constructor() {
        super("EvaProjectExtDirector");
    }
}