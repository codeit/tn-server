import { BaseDataMapper } from "./base-mapper.js";
import { sequelize } from "../lib/database.js";
import { EvaProjectUserMapper } from './eva-project-user.ts';
export class EvaAnswerMapper extends BaseDataMapper {
    declare userMapper: EvaProjectUserMapper;
    constructor() {
        super(`EvaAnswer`);
        this.userMapper = new EvaProjectUserMapper;
    }
    async submitAnswer(answers: any) {
        const records = [];
        let p_project_id, p_project_owner_id, p_name;
        for (const {
            project_id,
            owner_user_id,
            rule_form_id,
            user_id,
            rule_id,
            project_owner_id,
            project_owner_name,
            rule_form_name,
            user_name,
            user_type,
            rule_index,
            rule_name,
            rule_type,
            rule_top_id,
            rule_top_name,
            values } of answers) {
            records.push({
                project_id,
                owner_user_id,
                rule_form_id,
                user_id,
                rule_id,
                project_owner_id,
                project_owner_name,
                rule_form_name,
                user_name,
                user_type,
                rule_index,
                rule_name,
                rule_type,
                rule_top_id,
                rule_top_name,
                rule_value: JSON.stringify(values)
            })
            if (!p_project_id) p_project_id = project_id;
            if (!p_project_owner_id) p_project_owner_id = project_owner_id;
            if (!p_name) p_name = owner_user_id;
        }
        const t = await sequelize.transaction();
        try {
            const options = { transaction: t }
            if (records.length !== 0) {
                await this.saveBatch({ records }, options);
            }
            await this.userMapper.update({ values: { is_submit: 1, is_login: 1 }, where: { project_id: p_project_id, project_owner_id: p_project_owner_id, name: p_name } }, options);
            await t.commit();
            return {
                code: 200,
                message: "提交成功",
            }
        } catch (error) {
            await t.rollback();
            return {
                code: 500,
                message: error,
            }
        }


    }
}