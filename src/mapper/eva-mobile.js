
import { QueryTypes } from "sequelize";
import { sequelize } from "../lib/database.js";
import logger from "../lib/log4js.js";

class EvaMobileMapper {
    constructor() {

    }

    /**
     * 第一步:
     * @param  project_id
     * @param  project_owner_id 
     * @returns 当前主体可测评的表单
     * @example
     *     [{
     *         project_id, 
     *         project_owner_id,
     *         ru_form_id,
     *         ru_form_name,
     *         user_type,
     *     }]
     */
    async findVoteForms({ project_id, project_owner_id }) {
        const sql = `
            select 
                epof.project_id, 
                epof.project_owner_id,
                epo.owner_name_name project_owner_name,
                epof.ru_form_id,
                epof.ru_form_name,
                epof.user_type 
                from eva_project_owner_form epof 
                inner join eva_project_owner epo on epo.project_id = epof.project_id and epo.id = epof.project_owner_id 
                where epof.project_id=? and epof.project_owner_id=? 
        `;
        const result = await sequelize.query(sql, {
            replacements: [project_id, project_owner_id],
            type: QueryTypes.SELECT
        });
        if (!result || result.length === 0) return [];
        return result.map(({ project_id, project_owner_id, project_owner_name, ru_form_id, ru_form_name, user_type }) => ({
            project_id,
            project_owner_id,
            project_owner_name,
            ru_form_id,
            ru_form_name,
            user_type
        }));
    }

    /**
     * 第二步:
     * @param project_id 
     * @param project_owner_id
     * @param project_owner_form_id      
     * @returns 获取当前测评表内的用户信息(票数，详情)
     * @example
     *      [{
                project_id,
                project_owner_id,
                project_owner_form_id,
                user_id,
                user_type,
                user_name
            }]
     */
    async findProjectFormUser({ project_id, project_owner_id, project_owner_form_id }) {
        const form = await this.findFormGroupByProject({ project_id, project_owner_id, ru_form_id: project_owner_form_id });
        if (!form) throw new Error(`the form ${project_owner_form_id} was not found`)
        if (form.user_type == 3) {
            //领导班子评价表，需要对整体领导班子做评价
            return [{
                project_id,
                project_owner_id,
                project_owner_form_id,
                user_id: 'leader_group',
                user_type: 3,
                user_name: '领导班子'
            }]
        }
        if (form['user_type'] == 4) {
            return [{
                project_id,
                project_owner_id,
                project_owner_form_id,
                user_id: 'demo_eva',
                user_name: '单位民主评议',
                user_type: 4,
            }]
        }
        const sql = `
            select 
              epofu.project_id,
              epofu.project_owner_id,
              epofu.project_owner_form_id,
              epofu.user_id,
              epofu.user_name,
              epofu.user_type
            from eva_project_owner_form_user epofu 
            where epofu.project_id =? and epofu.project_owner_id =?
            and epofu.project_owner_form_id =?
            order by epofu.sort
        `;
        const result = await sequelize.query(sql, {
            replacements: [project_id, project_owner_id, project_owner_form_id],
            type: QueryTypes.SELECT
        });
        return result.map(({
            project_id,
            project_owner_id,
            project_owner_form_id,
            user_id,
            user_name,
            user_type
        }) => ({
            project_id,
            project_owner_id,
            project_owner_form_id,
            user_id,
            user_name,
            user_type
        }));

    }

    /***
     * 第三步:
     * @param form_id
     * @returns
     * 获取某个表单中评价指标列表
     */
    async findFormRuleDetails({ form_id }) {

        const sql = `
            select 
            rsr.id,
            rsr.name,
            rsr.rule_type ,
            rsr.is_required ,
            rsr.\`options\`,
            rsr.min_select,
            rsr.max_select,
            rsr.top_id,
            rsr.top_name
            from ru_form_rule rfr 
            inner join ru_second_rule rsr on rsr.id = rfr.second_id 
            where rfr.form_id =?
            order by rfr.sort 
        `;
        const result = await sequelize.query(sql, {
            replacements: [form_id],
            type: QueryTypes.SELECT
        });

        if (result && result.length != 0) {
            return result.map(({ id, name, rule_type, options, min_select, max_select, is_required }) => {
                if (rule_type === '打分') {
                    const items = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
                    return {
                        id,
                        name,
                        rule_type,
                        min_select,
                        max_select,
                        is_required,
                        items: items.map(u => ({ text: u, value: u }))
                    }
                }
                if (rule_type == '单选') {
                    return {
                        id,
                        name,
                        rule_type,
                        min_select,
                        max_select,
                        is_required,
                        items: options.split("\n").map(u => ({ text: u, value: u }))
                    }
                }
                if (rule_type == '多选') {
                    return {
                        id,
                        name,
                        rule_type,
                        min_select,
                        max_select,
                        is_required,
                        items: options.split("\n").map(u => ({ text: u, value: u })),

                    }
                }
                if (rule_type == '问答') {
                    return {
                        id,
                        name,
                        rule_type,
                        min_select,
                        max_select,
                        is_required,
                        items: options
                    }
                }
            })
        }
        return [];

    }

    /***
     * 辅助方法
     * 获取某个工程某个测评表信息，和单位信息
     */
    async findFormGroupByProject({ project_id, project_owner_id, ru_form_id }) {
        const sql = `
          select 
            epof.project_id, 
            epof.project_owner_id,
            epof.ru_form_id,
            epof.ru_form_name,
            epof.user_type,
            ep.object_group_id object_group_id ,
            ep.object_group_name 
            from eva_project_owner_form epof 
            inner join eva_project ep on ep.id  = epof.project_id 
            where epof.project_id=? and epof.project_owner_id=? and epof.ru_form_id=?  ; 
        `;
        const result = await sequelize.query(sql, {
            replacements: [project_id, project_owner_id, ru_form_id],
            type: QueryTypes.SELECT
        });
        return result && result.length > 0 ? result[0] : null;
    }

}

export { EvaMobileMapper }


