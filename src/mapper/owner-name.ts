
import { QueryTypes } from "sequelize";
import { sequelize } from "../lib/database.js";
import { BaseDataMapper } from "./base-mapper.js";
import logger from "../lib/log4js.js";
export class OwnerNameMapper extends BaseDataMapper {
    constructor() {
        super("OwnerName");
    }

    async findOwnerNameByProject(project_id: string) {
        const sql = `
           select on2.id,on2.name ,rfot.remark from ru_form_owner_type rfot 
inner join eva_project_form epf 
  on epf.project_id =? and epf.ru_form_id  = rfot.form_id 
inner join eva_project ep on ep.id=?
inner join owner_name on2 on on2.type_id  = rfot.type_id 
where rfot.group_type  = ep.group_type 
group by on2.id 
order by on2.name
        `;
        let result: any;
        try {
            result = await sequelize.query(sql, {
                replacements: [project_id, project_id],
                type: QueryTypes.SELECT
            });
        } catch (error) {
            logger.error(error);
        }
        if (!result || result.length === 0) {
            return []
        }
        return result;
    }
}