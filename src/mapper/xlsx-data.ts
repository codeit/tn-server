abstract class XlsxData {

    abstract exportXlsxSheet(test: boolean, project_id: string, rowNo: number, index: number): Promise<SheetMeta>;

}
export { XlsxData }