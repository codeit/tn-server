import { BaseDataMapper } from "./base-mapper.js";
export class EvaProjectLeaderMapper extends BaseDataMapper {
    constructor() {
        super("EvaProjectLeader");
    }
}