import { BaseDataMapper } from "./base-mapper.js";
export class OwnerTypeMapper extends BaseDataMapper {
    constructor() {
        super("OwnerType");
    }
}