import { QueryTypes } from "sequelize";
import { sequelize } from "../lib/database.js";
import logger from "../lib/log4js.js";
import { BaseDataMapper } from "./base-mapper.js";
export class RuFormRuleMapper extends BaseDataMapper {
    constructor() {
        super("RuFormRule");
    }
    async checkWeight(form_id: string) {

        if (!form_id) throw new Error("form_id can not be empty");
        const sql = "select sum(weight_value) weight_value from ru_form_rule where form_id= :formid "
        const weights = await sequelize.query(sql, {
            replacements: { formid: form_id },
            type: QueryTypes.SELECT,
        });
        if (!weights || weights.length === 0) return false;
        const weight_value = (weights[0] as any).weight_value;
        return (weight_value == 100);

    }
}