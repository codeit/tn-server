
import { QueryTypes } from "sequelize";
import { sequelize } from "../lib/database.js";
import { BaseDataMapper } from "./base-mapper.js";
import logger from "../lib/log4js.js";

export class RuFormOwnerTypeMapper extends BaseDataMapper {
    constructor() {
        super("RuFormOwnerType");
    }
    async checkWeight(form_id: string, group_type: number) {
        logger.debug(form_id)
        if (!form_id) throw new Error("form_id can not be empty");
        const sql = `select sum(weight_value) weight_value 
        from ru_form_owner_type where form_id= :formid and group_type= :gtype `
        const weights = await sequelize.query(sql, {
            replacements: { formid: form_id, gtype: group_type },
            type: QueryTypes.SELECT,
        });
        if (!weights || weights.length === 0) return false;
        const weight_value = (weights[0] as any).weight_value;
        return weight_value == 100;
    }
    async findFormsByOwnerType(project_id: string, project_owner_id: string) {
        const sql = `
        select rf.id,rf.name,rf.user_type from ru_form_owner_type rfot 
inner join eva_project ep on ep.id =?
inner join eva_project_owner epo on epo.project_id=? and epo.id =?
inner join owner_name on3 on on3.id = epo.owner_name_id and on3.type_id  = rfot.type_id 
inner join ru_form rf on rf.id  = rfot.form_id
where rfot.group_type =ep.group_type 
group by rf.id;
        `;
        const result = await sequelize.query(sql, {
            replacements: [project_id, project_id, project_owner_id],
            type: QueryTypes.SELECT,
        });

        return result;
    }
}