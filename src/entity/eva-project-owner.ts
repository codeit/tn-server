import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class EvaProjectOwner extends Model {
  declare owner_name_id: string;
  declare owner_name_name: string;

}
function create() {
  EvaProjectOwner.init(
    {
      id: {
        type: DataTypes.STRING,
        defaultValue: sequelize.literal('uuid()'),
        primaryKey: true,
      },
      project_id: DataTypes.STRING,
      owner_name_id: {
        type: DataTypes.STRING,
        comment: "主体名称",
      },
      owner_name_name: DataTypes.STRING,
      plan_quantity: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        comment: "计划数量"
      },
      remark: DataTypes.STRING,
      sort: {
        type: DataTypes.SMALLINT,
        defaultValue: 0
      },
      owner_name_info: {
        type: DataTypes.VIRTUAL,
        get() {
          return `${this.owner_name_id}^${this.owner_name_name}`
        },
        set(value) {

        }
      }
    },
    {
      sequelize,
      timestamps: true,
      createdAt: false,
      updatedAt: false,
      tableName: "eva_project_owner",
      schema: "hrevaluate",
      comment: "项目测评主体"
    },
  );
}
export default {
  model: EvaProjectOwner,
  create
}
