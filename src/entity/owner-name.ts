import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class OwnerName extends Model {
    declare id: string | undefined;
    declare type_id: string;
    declare type_name: string;
}
function create() {
    OwnerName.init(
        {
            id: {
                type: DataTypes.STRING,
                defaultValue: sequelize.literal('uuid()'),
                primaryKey: true,
            },
            name: {
                type: DataTypes.STRING,
            },
            type_id: DataTypes.STRING,
            type_name: DataTypes.STRING,
            add_ticks: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
                comment: "追加票数"
            },
            remark: DataTypes.STRING,
            sort: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            status: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            type_info: {
                type: DataTypes.VIRTUAL,
                get() {
                    return `${this.type_id}^${this.type_name}`
                },
                set(value) { }
            }
        },
        {
            sequelize,
            timestamps: true,
            createdAt: false,
            updatedAt: false,
            tableName: "owner_name",
            schema: "hrevaluate",
            comment: "测评主体名称"
        },
    );
}
export default {
    model: OwnerName,
    create
}
