/***
 * 注意，管理员在添加指标时要和本条目名称保持一致
 * 
 */
// 0:领导人员
const A0: string[] = [
    "政治能力", "政治表现",
    "创新精神", "创新成果",
    "专业素养", "领导能力",
    "担当作为", "履职绩效",
    "一岗双责", "廉洁从业"]
const A0_TOP: RuleTop = {
    "对党忠诚": { items: ["政治能力", "政治表现"], rate: 20, child_rate: [50, 50] },
    "勇于创新": { items: ["创新精神", "创新成果"], rate: 15, child_rate: [40, 60] },
    "治企有方": { items: ["专业素养", "领导能力"], rate: 15, child_rate: [50, 50] },
    "兴企有为": { items: ["担当作为", "履职绩效"], rate: 35, child_rate: [30, 70] },
    "清正廉洁": { items: ["一岗双责", "廉洁从业"], rate: 15, child_rate: [50, 50] },
};

// 3:领导班子
const D3: string[] = [
    "政治担当", "社会责任",
    "经营效益", "管理效能", "风险管控",
    "科技创新", "人才强企", "深化改革",
    "选人用人", "基层党建", "党风廉政",
    "团结协作", "联系群众"];

const D3_TOP: RuleTop = {
    "思想政治建设": { items: ["政治担当", "社会责任"], rate: 15, child_rate: [80, 20] },
    "经营发展": { items: ["经营效益", "管理效能", "风险管控"], rate: 30, child_rate: [60, 20, 20] },
    "改革创新": { items: ["科技创新", "人才强企", "深化改革"], rate: 30, child_rate: [33, 33, 34] },
    "党建工作": { items: ["选人用人", "基层党建", "党风廉政"], rate: 15, child_rate: [40, 30, 30] },
    "作风建设": { items: ["团结协作", "联系群众"], rate: 10, child_rate: [70, 30] }
};

// 1:新提拔干部
const B1: string[] = ["对提拔该中层领导人员的看法", "您对新提拔任用的同志表示不认同的主要原因是"]
// 2:外部董事
const C2: string[] = ["忠实勤勉", "严于律己", "科学决策", "监督问效", "建言献策"]
const C2_TOP: RuleTop = {
    "行为操守": { items: ["忠实勤勉", "严于律己"], rate: 40, child_rate: [62, 38] },
    "履职贡献": { items: ["科学决策", "监督问效", "建言献策"], rate: 60, child_rate: [50, 35, 15] },
}
// 4:选人用人民主评议
const E4: string[] = ["对本单位选人用人工作的总体评价", "对本单位从严管理监督干部情况的看法?", "您认为本单位选人用人工作存在的主要问题是什么?", "您对加强和改进本单位选人用人工作有何意见建议?"]
const USER_TYPE_DEFINES: string[] = ["领导干部", "新提拔干部", "外部董事", "领导班子", "选人用人民主评议", "领导政治素质"];

enum USER_TYPES {
    LEADERS = 0,
    NEW_CADRE = 1,
    EXT_DIRECTOR = 2,
    LEADER_GROUP = 3,
    DEMO_EVA = 4,
    POLITICAL_QUALITY = 5
};
enum GROUP_TYPE {
    NONE_DIRECTOR = 0,
    DIRECTOR = 1,
}
export { A0, A0_TOP, B1, C2, C2_TOP, D3, D3_TOP, E4, USER_TYPE_DEFINES, USER_TYPES, GROUP_TYPE }
