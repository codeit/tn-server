import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class ObjectLeaderType extends Model { }
function create() {
    ObjectLeaderType.init(
        {
            id: {
                type: DataTypes.STRING,
                defaultValue: sequelize.literal('uuid()'),
                primaryKey: true,
            },
            name: DataTypes.STRING,
            remark: DataTypes.STRING,
            sort: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            status: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
        },
        {
            sequelize,
            timestamps: true,
            createdAt: false,
            updatedAt: false,
            tableName: "object_leader_type",
            schema: "hrevaluate",
            comment: "领导人员类别"
        },
    );
}
export default {
    model: ObjectLeaderType,
    create
}
