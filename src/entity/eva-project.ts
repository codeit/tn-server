import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
import { build_virtual_value } from "../lib/util.js";
import { GROUP_TYPE } from "./common-defines.ts";
class EvaProject extends Model {
    declare object_group_id: string;
    declare object_group_name: string;
    declare end_time: Date;
}
function create() {
    EvaProject.init(
        {
            id: {
                type: DataTypes.STRING,
                defaultValue: sequelize.literal('uuid()'),
                primaryKey: true,
            },
            object_group_id: DataTypes.STRING,
            object_group_name: DataTypes.STRING,
            name: DataTypes.STRING,
            ef_year: DataTypes.STRING,
            start_time: DataTypes.DATE,
            end_time: DataTypes.DATE,
            is_start: {
                type: DataTypes.SMALLINT,
                defaultValue: 0,
            },
            limit_time: {
                type: DataTypes.BIGINT,
                comment: "时间限制[小时]，转换为millisecond"
            },
            remark: DataTypes.STRING,
            group_type: {
                type: DataTypes.TINYINT,
                defaultValue: GROUP_TYPE.DIRECTOR,
            },
            object_group_info: {
                type: DataTypes.VIRTUAL,
                get() {
                    return build_virtual_value([this.object_group_id, this.object_group_name])
                },
                set(value) { }
            },
            is_finished: {
                type: DataTypes.VIRTUAL,
                get() {
                    return !this.end_time ? false : ((this.end_time.getTime() + 86400000) < new Date().getTime());
                },
                set(value) { }
            }
        },
        {
            sequelize,
            timestamps: true,
            createdAt: false,
            updatedAt: false,
            tableName: "eva_project",
            schema: "hrevaluate",
            comment: "评测项目"
        },
    );
}
export default {
    model: EvaProject,
    create
}
