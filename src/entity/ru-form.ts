import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class RuForm extends Model { }
function create() {
    RuForm.init(
        {
            id: {
                type: DataTypes.STRING,
                defaultValue: sequelize.literal('uuid()'),
                primaryKey: true,
            },
            name: DataTypes.STRING,
            remark: DataTypes.STRING,
            user_type: {
                type: DataTypes.INTEGER,
                defaultValue: -1,
                comment: "0:领导人员,1:新提拔干部,2:外部董事,3:领导班子,4:选人用人民主评议,5:领导政治素质"
            },
            use_entire: {
                type: DataTypes.SMALLINT,
                defaultValue: 0,
            },
            entire_title: {
                type: DataTypes.STRING,
            },
            sort: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            status: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
        },
        {
            sequelize,
            timestamps: true,
            createdAt: false,
            updatedAt: false,
            tableName: "ru_form",
            schema: "hrevaluate",
            comment: "考核表"
        },
    );
}
export default {
    model: RuForm,
    create
}
