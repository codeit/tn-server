import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class EvaProjectOwnerFormUser extends Model { }
function create() {
  EvaProjectOwnerFormUser.init(
    {
      project_id: {
        type: DataTypes.STRING,
        primaryKey: true,
        comment: "所属评测项目"
      },
      project_owner_id: {
        type: DataTypes.STRING,
        primaryKey: true,
        comment: "所属主体"
      },
      project_owner_form_id: {
        type: DataTypes.STRING,
        primaryKey: true,
        comment: "项目所属评测表"
      },
      user_id: {
        type: DataTypes.STRING,
        primaryKey: true,
        comment: "此表此项目可评测人员",
      },
      user_name: DataTypes.STRING,
      user_type: {
        type: DataTypes.INTEGER,
        comment: "用户类型,0:leader,1:new-leader,2:ext-director"
      },
      sort: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
    },
    {
      sequelize,
      timestamps: true,
      createdAt: false,
      updatedAt: false,
      tableName: "eva_project_owner_form_user",
      schema: "hrevaluate",
      comment: "测评项目的测评主体表-可测评人员表,在项目各类人员中选择"
    },
  );
}
export default {
  model: EvaProjectOwnerFormUser,
  create
}
