import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class ObjectGroup extends Model { }
function create() {
    ObjectGroup.init(
        {
            id: {
                type: DataTypes.STRING,
                defaultValue: sequelize.literal('uuid()'),
                primaryKey: true,
            },
            name: DataTypes.STRING,
            short_name: DataTypes.STRING,
            type: DataTypes.STRING,
            remark: DataTypes.STRING,
            sort: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            status: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
        },
        {
            sequelize,
            timestamps: true,
            createdAt: false,
            updatedAt: false,
            tableName: "object_group",
            schema: "hrevaluate",
            comment: "测评单位"
        },
    );
}
export default {
    model: ObjectGroup,
    create
}
