import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
import { build_virtual_value } from "../lib/util.js";
class RuFormOwnerType extends Model {
    declare type_id: string;
    declare type_name: string;
}
function create() {
    RuFormOwnerType.init(
        {
            form_id: {
                type: DataTypes.STRING,
                primaryKey: true,
            },
            type_id: {
                type: DataTypes.STRING,
                primaryKey: true,
            },
            group_type: {
                type: DataTypes.TINYINT,
                primaryKey: true,
            },
            type_name: DataTypes.STRING,
            weight_value: DataTypes.INTEGER,

            remark: DataTypes.STRING,
            sort: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            status: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            type_info: {
                type: DataTypes.VIRTUAL,
                get() {
                    return build_virtual_value([this.type_id, this.type_name])
                },
                set(value) { }
            }

        },
        {
            sequelize,
            timestamps: true,
            createdAt: false,
            updatedAt: false,
            tableName: "ru_form_owner_type",
            schema: "hrevaluate",
            comment: "考核表主体类别及权重"
        },
    );
}
export default {
    model: RuFormOwnerType,
    create
}
