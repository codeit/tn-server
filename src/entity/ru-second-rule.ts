import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class RuSecondRule extends Model {
    declare top_id: string;
    declare top_name: string;
}
function create() {
    RuSecondRule.init(
        {
            id: {
                type: DataTypes.STRING,
                defaultValue: sequelize.literal('uuid()'),
                primaryKey: true,
            },
            top_id: DataTypes.STRING,
            top_name: DataTypes.STRING,
            eva_target: {
                type: DataTypes.STRING,
                comment: "评价对象"
            },
            rule_type: {
                type: DataTypes.STRING,
                comment: "指标类型：打分,单选,多选,问答"
            },
            is_required: {
                type: DataTypes.BOOLEAN,
                defaultValue: true,
            },
            name: DataTypes.STRING,
            remark: DataTypes.STRING,
            options: {
                type: DataTypes.STRING,
                comment: "选项,每行一个"
            },
            values: {
                type: DataTypes.STRING,
                comment: "选择的分值"
            },
            min_select: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
                comment: "最少选择",
            },
            max_select: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
                comment: "最大选择"
            },
            sort: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            status: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            top_info: {
                type: DataTypes.VIRTUAL,
                get() {
                    return this.top_id + "^" + this.top_name;
                },
                set(value) {

                }
            }
        },
        {
            sequelize,
            timestamps: true,
            createdAt: false,
            updatedAt: false,
            tableName: "ru_second_rule",
            schema: "hrevaluate",
            comment: "二级指标"
        },
    );

}
export default {
    model: RuSecondRule,
    create
}
