
import adminUser from "./admin-user.ts"
import evaAnswer from "./eva-answer.ts"
import evaProjectExtDirector from "./eva-project-ext-director.ts"
import evaProjectForm from "./eva-project-form.ts"
import evaProjectLeader from "./eva-project-leader.ts"
import evaProjectNewLeader from "./eva-project-new-leader.ts"
import evaProjectOwnerFormUser from "./eva-project-owner-form-user.ts"
import evaProjectOwnerForm from "./eva-project-owner-form.ts"
import evaProjectOwner from "./eva-project-owner.ts"
import evaProject from "./eva-project.ts"
import evaUser from "./eva-project-user.ts"
import objectGroup from "./object-group.ts"
import objectLeaderType from "./object-leader-type.ts"
import evaOwnerName from "./owner-name.ts"
import evaOwnerType from "./owner-type.ts"
import ruFormOwnerType from "./ru-form-owner-type.ts"
import ruFormRule from "./ru-form-rule.ts"
import ruForm from "./ru-form.ts"
import ruSecondRule from "./ru-second-rule.ts"
import ruTopRule from "./ru-top-rule.ts"
import logger from "../lib/log4js.js"
import { sequelize } from "../lib/database.js"
const models = [
    adminUser,
    evaAnswer,
    evaProjectExtDirector,
    evaProjectForm,
    evaProjectLeader,
    evaProjectNewLeader,
    evaProjectOwnerFormUser,
    evaProjectOwnerForm,
    evaProjectOwner,
    evaProject,
    evaUser,
    objectGroup,
    objectLeaderType,
    evaOwnerName,
    evaOwnerType,
    ruFormOwnerType,
    ruFormRule,
    ruForm,
    ruSecondRule,
    ruTopRule
];
async function initModels() {
    logger.debug("models creating...")
    models.forEach(m => {
        logger.debug(`Model ${m.model.name} created`);
        m.create();
    })
    logger.info("models created")
    await sequelize.sync({ force: false });
}
export { initModels };