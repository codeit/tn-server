import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class EvaProjectLeader extends Model {
  declare type_id: string;
  declare type_name: string;
}
function create() {
  EvaProjectLeader.init(
    {
      id: {
        type: DataTypes.STRING,
        defaultValue: sequelize.literal('uuid()'),
        primaryKey: true,
      },
      project_id: DataTypes.STRING,
      name: {
        type: DataTypes.STRING,
      },
      unique_id: {
        type: DataTypes.STRING,
        comment: "唯一标识"
      },
      category: {
        type: DataTypes.STRING,
        comment: "类别"
      },
      type_id: {
        type: DataTypes.STRING,
        comment: "人员类型"
      },
      type_name: DataTypes.STRING,
      role_name: DataTypes.STRING,
      bod: DataTypes.STRING,
      remark: DataTypes.STRING,
      sort: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      type_info: {
        type: DataTypes.VIRTUAL,
        get() {
          return `${this.type_id}^${this.type_name}`;
        },
        set(value) { }
      }
    },

    {
      sequelize,
      timestamps: true,
      createdAt: false,
      updatedAt: false,
      tableName: "eva_project_leader",
      schema: "hrevaluate",
      comment: "评测项目领导人员"
    },
  );
}
export default {
  model: EvaProjectLeader,
  create
}
