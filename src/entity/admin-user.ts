import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
import useBcrypt from 'sequelize-bcrypt';
import { build_virtual_value } from "../lib/util.js";
import logger from "../lib/log4js.js";
class AdminUser extends Model {
  declare store_pass: string;
  declare group_id: string;
  declare group_name: string;
}
function create() {
  AdminUser.init(
    {
      name: {
        type: DataTypes.STRING,
        primaryKey: true
      },
      store_pass: DataTypes.STRING,
      phone_num: DataTypes.STRING,
      group_id: DataTypes.STRING,
      group_name: DataTypes.STRING,
      user_type: {
        type: DataTypes.TINYINT,
        defaultValue: 1,
        comment: "账户类型 0 一级管理员,1 项目管理员"
      },
      group_info: {
        type: DataTypes.VIRTUAL,
        get() { return build_virtual_value([this.group_id, this.group_name]) },
        set(value) { }
      }
    },
    {
      sequelize,
      timestamps: true,
      createdAt: false,
      updatedAt: false,
      tableName: "tn_admin_user",
      schema: "hrevaluate",
      comment: "用户"
    },
  );
  useBcrypt(AdminUser, {
    field: 'store_pass',
  })
}
export default {
  model: AdminUser,
  create
}
