import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class EvaAnswer extends Model {
}
function create() {
    EvaAnswer.init(
        {
            project_id: {
                type: DataTypes.STRING(40),
                primaryKey: true,
                comment: "所属项目"
            },
            owner_user_id: {
                type: DataTypes.STRING(40),
                primaryKey: true,
                comment: "主体用户"
            },
            rule_form_id: {
                type: DataTypes.STRING(40),
                primaryKey: true,
                comment: "所属评测表"
            },
            user_id: {
                type: DataTypes.STRING(50),
                primaryKey: true,
                comment: "被测评用户"
            },
            rule_id: {
                type: DataTypes.STRING(40),
                primaryKey: true,
            },
            project_owner_id: {
                type: DataTypes.STRING(40),
                comment: "所在主体"
            },
            project_owner_name: DataTypes.STRING,
            rule_form_name: DataTypes.STRING,
            user_name: DataTypes.STRING,
            user_type: DataTypes.SMALLINT,
            rule_index: DataTypes.SMALLINT,
            is_test: {
                type: DataTypes.TINYINT,
                defaultValue: 0
            },
            rule_name: {
                type: DataTypes.STRING,

            },
            rule_type: {
                type: DataTypes.STRING,
            },
            rule_value: {
                type: DataTypes.STRING,
                comment: "选项值"
            }

        },
        {
            sequelize,
            timestamps: true,
            createdAt: false,
            updatedAt: false,
            tableName: "eva_answer",
            schema: "hrevaluate",
            comment: "领导人员表评测答案"
        },
    );
}
export default {
    model: EvaAnswer,
    create
}


