import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class EvaProjectUser extends Model {
    declare project_owner_name_name: string;
    declare sort: number;
}
function create() {
    EvaProjectUser.init(
        {
            project_id: {
                type: DataTypes.STRING,
                primaryKey: true,
                comment: "所属测评项目"
            },
            project_owner_id: {
                type: DataTypes.STRING,
                primaryKey: true,
                comment: "所属项目主体ID,关联eva_project_owner表主键",
            },
            name: {
                type: DataTypes.STRING,
                primaryKey: true,
                comment: "用户主体账户"
            },
            code: {
                type: DataTypes.STRING,
                comment: "账户验证码",
            },
            project_owner_name: DataTypes.STRING,
            user_type: DataTypes.STRING,
            is_login: DataTypes.INTEGER,
            is_submit: DataTypes.INTEGER,
            is_test: {
                type: DataTypes.TINYINT,
                defaultValue: 0
            },
            sort: DataTypes.INTEGER,

        },
        {
            sequelize,
            timestamps: true,
            createdAt: false,
            updatedAt: false,
            tableName: "eva_project_user",
            schema: "hrevaluate",
            comment: "项目测评人员"
        },
    );
}
export default {
    model: EvaProjectUser,
    create
}
