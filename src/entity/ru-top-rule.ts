import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
import logger from "../lib/log4js.js";
class RuTopRule extends Model { }
function create() {
    RuTopRule.init(
        {
            id: {
                type: DataTypes.STRING,
                defaultValue: sequelize.literal('uuid()'),
                primaryKey: true,
            },
            name: DataTypes.STRING,
            remark: DataTypes.STRING,
            sort: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            status: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
        },
        {
            sequelize,
            timestamps: true,
            createdAt: false,
            updatedAt: false,
            tableName: "ru_top_rule",
            schema: "hrevaluate",
            comment: "一级指标"
        },
    );
}
export default {
    model: RuTopRule,
    create
}
