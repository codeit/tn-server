import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
import { build_virtual_value } from "../lib/util.js";
class EvaProjectOwnerForm extends Model {
  declare ru_form_id: string;
  declare ru_form_name: string;
  declare user_type: number;
}
function create() {
  EvaProjectOwnerForm.init(
    {
      project_id: {
        type: DataTypes.STRING,
        primaryKey: true,
        comment: "所属项目"
      },
      project_owner_id: {
        type: DataTypes.STRING,
        primaryKey: true,
        comment: "所属测评主体"
      },
      ru_form_id: {
        type: DataTypes.STRING,
        primaryKey: true,
        comment: "测评表",
      },
      ru_form_name: DataTypes.STRING,
      user_type: {
        type: DataTypes.INTEGER,
        comment: "表单用户类型",
      },
      ru_form_info: {
        type: DataTypes.VIRTUAL,
        get() {
          return build_virtual_value([this.ru_form_id, this.ru_form_name, this.user_type]);
        },
        set(value) { }
      }
    },
    {
      sequelize,
      timestamps: true,
      createdAt: false,
      updatedAt: false,
      tableName: "eva_project_owner_form",
      schema: "hrevaluate",
      comment: "项目测评主体<->可测评表关系"
    },
  );
}
export default {
  model: EvaProjectOwnerForm,
  create
}
