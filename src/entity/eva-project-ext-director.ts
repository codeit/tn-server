import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class EvaProjectExtDirector extends Model { }
function create() {
  EvaProjectExtDirector.init(
    {
      id: {
        type: DataTypes.STRING,
        defaultValue: sequelize.literal('uuid()'),
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
      },
      project_id: DataTypes.STRING,
      unique_id: {
        type: DataTypes.STRING,
        comment: "唯一标识"
      },
      sort: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      role_type: DataTypes.STRING,
      bod: DataTypes.STRING,
      remark: DataTypes.STRING,
    },
    {
      sequelize,
      timestamps: true,
      createdAt: false,
      updatedAt: false,
      tableName: "eva_project_ext_director",
      schema: "hrevaluate",
      comment: "评测项目外部董事"
    },
  );
}
export default {
  model: EvaProjectExtDirector,
  create
}
