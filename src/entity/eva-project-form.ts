import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
import { build_virtual_value } from "../lib/util.js";
import { USER_TYPE_DEFINES } from "./common-defines.ts";
class EvaProjectForm extends Model {
  declare user_type: number;
  declare ru_form_id: string;
  declare ru_form_name: string;
}
function create() {
  EvaProjectForm.init(
    {
      project_id: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      ru_form_id: {
        type: DataTypes.STRING,
        primaryKey: true,
        comment: "测评表编号",
      },
      ru_form_name: DataTypes.STRING,
      user_type: {
        type: DataTypes.INTEGER,
        defaultValue: -1,
      },
      ru_form_info: {
        type: DataTypes.VIRTUAL,
        get() {
          return build_virtual_value([this.ru_form_id, this.ru_form_name, this.user_type])
        },
        set(value) { }
      },
      user_type_name: {
        type: DataTypes.VIRTUAL,
        get() {
          return USER_TYPE_DEFINES[this.user_type];
        },
        set(value) {

        }
      }
    },
    {
      sequelize,
      timestamps: true,
      createdAt: false,
      updatedAt: false,
      tableName: "eva_project_form",
      schema: "hrevaluate",
      comment: "测评项目可用测评表设置"
    },
  );
}
export default {
  model: EvaProjectForm,
  create
}
