import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class EvaProjectNewLeader extends Model { }
function create() {
  EvaProjectNewLeader.init(
    {
      id: {
        type: DataTypes.STRING,
        defaultValue: sequelize.literal('uuid()'),
        primaryKey: true,
      },
      project_id: DataTypes.STRING,
      name: {
        type: DataTypes.STRING,
      },

      unique_id: {
        type: DataTypes.STRING,
        comment: "唯一标识"
      },
      sort: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      gender: DataTypes.STRING,
      duty_prev: DataTypes.STRING,
      duty_current: DataTypes.STRING,
      duty_date: DataTypes.STRING,
      bod: DataTypes.STRING,
      remark: DataTypes.STRING,
    },
    {
      sequelize,
      timestamps: true,
      createdAt: false,
      updatedAt: false,
      tableName: "eva_project_new_leader",
      schema: "hrevaluate",
      comment: "评测项目新提拔干部"
    },
  );
}
export default {
  model: EvaProjectNewLeader,
  create
}
