import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class RuFormRule extends Model {
    declare second_id: string;
    declare second_name: string;
    declare top_id: string;
    declare top_name: string;
}
function create() {
    RuFormRule.init(
        {
            form_id: {
                type: DataTypes.STRING,
                primaryKey: true,
                comment: "测评表"
            },
            top_id: {
                type: DataTypes.STRING,
                primaryKey: true,
                comment: "一级指标"
            },
            second_id: {
                type: DataTypes.STRING,
                primaryKey: true,
                comment: "二级指标"
            },
            top_name: DataTypes.STRING,
            second_name: DataTypes.STRING,
            use_weight: DataTypes.STRING,
            weight_value: {
                type: DataTypes.STRING,
                comment: "指标权重"
            },
            sort: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            status: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            second_info: {
                type: DataTypes.VIRTUAL,
                get() {
                    return `${this.second_id}^${this.second_name}^${this.top_id}^${this.top_name}`
                },
                set(value) { }
            }
        },
        {
            sequelize,
            timestamps: true,
            createdAt: false,
            updatedAt: false,
            tableName: "ru_form_rule",
            schema: "hrevaluate",
            comment: "考核表所有指标"
        },
    );
}
export default {
    model: RuFormRule,
    create
}
