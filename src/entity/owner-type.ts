import { DataTypes, Model } from "sequelize";
import { sequelize } from "../lib/database.js";
class OwnerType extends Model { }
function create() {
    OwnerType.init(
        {
            id: {
                type: DataTypes.STRING,
                defaultValue: sequelize.literal('uuid()'),
                primaryKey: true,
            },
            name: DataTypes.STRING,
            remark: DataTypes.STRING,
            sort: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
            status: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
            },
        },
        {
            sequelize,
            timestamps: true,
            createdAt: false,
            updatedAt: false,
            tableName: "owner_type",
            schema: "hrevaluate",
            comment: "测评主体类别"
        },
    );
}
export default {
    model: OwnerType,
    create
}
