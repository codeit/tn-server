import Koa from "koa";
import json from 'koa-json'
import session from 'koa-session';
import cors from '@koa/cors';
import bodyParser from 'koa-bodyparser';
import { initModels } from "./entity/index.ts";
import { initRoutes } from "./router/index.js";
import logger from "./lib/log4js.js";
const app = new Koa();
app.use(bodyParser());
const CONFIG: any = {
   key: 'koa.sess',
   maxAge: 86400000,
   autoCommit: true,
   overwrite: true,
   httpOnly: true,
   signed: true,
   rolling: false,
   renew: false,
   secure: true,
   sameSite: null,
};

app.use(json());
app.use(cors());
app.use(session(CONFIG, app));
initModels();
initRoutes(app);
const port = Deno.env.get("PORT");
const mode = Deno.env.get("MODE") || "dev";
app.listen(port);
logger.info(`Started server listening on:${port} as ${mode}`)