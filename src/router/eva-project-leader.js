import { BaseRouter } from "./base-router.js";
import { EvaProjectLeaderMapper } from "../mapper/eva-project-leader.ts";
export class EvaProjectLeaderRouter extends BaseRouter {
    constructor() {
        super(new EvaProjectLeaderMapper())
    }
}