import { AdminUserRouter } from "./admin-user.js";
import { EvaAnswerRouter } from "./eva-answer.js";
import { EvaMobileRouter } from "./eva-mobile.js";
import { EvaProjectExtDirectorRouter } from "./eva-project-ext-director.js";
import { EvaProjectFormRouter } from "./eva-project-form.js";
import { EvaProjectLeaderRouter } from "./eva-project-leader.js";
import { EvaProjectNewLeaderRouter } from "./eva-project-new-leader.js";
import { EvaProjectOwnerFormRouter } from "./eva-project-owner-form.js";
import { EvaProjectOwnerFormUserRouter } from "./eva-project-owner-form-user.js";
import { EvaProjectOwnerRouter } from "./eva-project-owner.js";
import { EvaProjectRouter } from "./eva-project.js";
import { EvaProjectUserRouter } from "./eva-project-user.js";
import { ObjectGroupRouter } from "./object-group.js";
import { ObjectLeaderTypeRouter } from "./object-leader-type.js";
import { OwnerNameRouter } from "./owner-name.js";
import { OwnerTypeRouter } from "./owner-type.js";
import { RuFormOwnerTypeRouter } from "./ru-form-owner-type.js";
import { RuFormRuleRouter } from "./ru-form-rule.js";
import { RuFormRouter } from "./ru-form.js";
import { RuSecondRuleRouter } from "./ru-second-rule.js";
import { RuTopRuleRouter } from "./ru-top-rule.js";
import { ResultLeaderGroupRouter } from "./result-leader-group.js";
import { ResultLeadersRouter } from "./result-leaders.js";
import { ResultExtDirectorRouter } from "./result-ext-director.js";
import { ResultRouter } from "./result.js";
import { ResultDemoEvaRouter } from "./result-demo-eva.js";
import { ResultNewCadreRouter } from "./result-new-cadre.js";
import logger from "../lib/log4js.js";
export function initRoutes(app) {
    const routers = [
        new AdminUserRouter,
        new EvaAnswerRouter,
        new EvaMobileRouter,
        new EvaProjectExtDirectorRouter,
        new EvaProjectFormRouter,
        new EvaProjectLeaderRouter,
        new EvaProjectNewLeaderRouter,
        new EvaProjectOwnerFormUserRouter,
        new EvaProjectOwnerFormRouter,
        new EvaProjectOwnerRouter,
        new EvaProjectUserRouter,
        new EvaProjectRouter,
        new ObjectGroupRouter,
        new ObjectLeaderTypeRouter,
        new OwnerNameRouter,
        new OwnerTypeRouter,
        new RuFormOwnerTypeRouter,
        new RuFormRuleRouter,
        new RuFormRouter,
        new RuSecondRuleRouter,
        new RuTopRuleRouter,
        new ResultLeaderGroupRouter,
        new ResultLeadersRouter,
        new ResultExtDirectorRouter,
        new ResultRouter,
        new ResultDemoEvaRouter,
        new ResultNewCadreRouter,
    ]
    logger.debug("routers creating...")
    for (const router of routers) {
        logger.debug(`Router[${router.prefix}] routes created`);
        app.use(router.getRouter().routes());
    }
    logger.info("routers created");
}