import { BaseRouter } from "./base-router.js";
import { EvaProjectFormMapper } from "../mapper/eva-project-form.ts";
export class EvaProjectFormRouter extends BaseRouter {
    constructor() {
        super(new EvaProjectFormMapper())
    }
}