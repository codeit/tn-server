import { BaseRouter } from "./base-router.js";
import { RuFormOwnerTypeMapper } from "../mapper/ru-form-owner-type.ts";
import { buildWhere } from "../lib/database.js";
export class RuFormOwnerTypeRouter extends BaseRouter {
    constructor() {
        super(new RuFormOwnerTypeMapper())
    }
    makeRouter(router) {
        router.get("/find-forms-by-type", async (ctx) => {
            const { where } = ctx.request.query;
            const { project_id, project_owner_id } = buildWhere(where);
            const result = await this.mapper.findFormsByOwnerType(project_id, project_owner_id);
            ctx.body = {
                success: true,
                code: 200,
                data: result
            }
        })
        router.post("/check-weight", async (ctx) => {
            const { where } = ctx.request.body;
            const { form_id, group_type } = where;

            const result = await this.mapper.checkWeight(form_id, group_type);
            ctx.body = {
                success: result,
                code: result ? 200 : 500,
                message: result ? "权重值符合要求" : "权重不符合要求"
            }
        })

    }
}