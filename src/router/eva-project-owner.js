import { BaseRouter } from "./base-router.js";
import { EvaProjectOwnerMapper } from "../mapper/eva-project-owner.ts";
export class EvaProjectOwnerRouter extends BaseRouter {
    constructor() {
        super(new EvaProjectOwnerMapper())
    }
}