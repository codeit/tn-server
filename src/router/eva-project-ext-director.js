import { BaseRouter } from "./base-router.js";
import { EvaProjectExtDirectorMapper } from "../mapper/eva-project-ext-director.ts";
export class EvaProjectExtDirectorRouter extends BaseRouter {
    constructor() {
        super(new EvaProjectExtDirectorMapper())
    }
}