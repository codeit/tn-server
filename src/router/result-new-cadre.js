import Router from '@koa/router';
import { ResultNewCadreMapper } from '../mapper/result-new-cadre.ts';
import { getExcelResult } from './result-base.js';
export class ResultNewCadreRouter {
    router = new Router({ prefix: '/result-new-cadre' });
    resultMapper = new ResultNewCadreMapper;

    getRouter() {
        this.router.get("/result", async (ctx) => {
            await getExcelResult(this.resultMapper, ctx);
        })
        return this.router;
    }


}