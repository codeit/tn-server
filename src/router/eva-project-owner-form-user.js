import { BaseRouter } from "./base-router.js";
import { EvaProjectOwnerFormUserMapper } from "../mapper/eva-project-owner-form-user.ts";
export class EvaProjectOwnerFormUserRouter extends BaseRouter {
    constructor() {
        super(new EvaProjectOwnerFormUserMapper())
    }
}