import { BaseRouter } from "./base-router.js";
import { buildWhere } from "../lib/database.js";
import { EvaProjectUserMapper } from "../mapper/eva-project-user.ts";
import logger from "../lib/log4js.js";
export class EvaProjectUserRouter extends BaseRouter {
    constructor() {
        super(new EvaProjectUserMapper())
    }
    getRouter() {
        const router = super.getRouter();
        router.get("/find-owned-user", async (ctx) => {
            const { where } = ctx.request.query;
            const { project_id, test } = buildWhere(where);
            logger.debug(project_id)
            const result = await this.mapper.findOwnedUser(project_id, test);
            ctx.body = {
                success: true,
                code: 200,
                data: result,
            }
        })
        router.post("/generate-user", async (ctx) => {
            const { where } = ctx.request.body;
            const { project_id, project_owner_id, amount, user_only, test } = buildWhere(where);
            if (!project_id) {
                ctx.body = {
                    success: false,
                    code: 500,
                    message: "错误的项目编号"
                }
                return;
            }
            const users = await this.mapper.generateOwnedUsers(project_id, project_owner_id, amount, user_only, test);
            ctx.body = {
                success: true,
                code: 200,
                data: users
            }
        })
        router.get("/check-user-code", async (ctx) => {
            const { where } = ctx.request.query;
            const { project_id, project_owner_id, name } = buildWhere(where);
            const result = await this.mapper.findCheckedUser(project_id, project_owner_id, name);
            ctx.body = {
                data: result,
            }
        })
        return router;
    }
}