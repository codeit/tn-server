import { BaseRouter } from "./base-router.js";
import { EvaProjectOwnerFormMapper } from "../mapper/eva-project-owner-form.ts";
export class EvaProjectOwnerFormRouter extends BaseRouter {
    constructor() {
        super(new EvaProjectOwnerFormMapper())
    }
}