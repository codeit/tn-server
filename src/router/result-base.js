import { buildWhere } from "../lib/database.js";
import logger from "../lib/log4js.js";
import { EvaProjectMapper } from "../mapper/eva-project.ts";
import * as XLSX from 'xlsx';

const getExcelResult = async (mapper, ctx) => {
    const { where } = ctx.request.query;
    const { project_id, is_test } = buildWhere(where);
    const { merges, data, name } = await mapper.exportXlsxSheet(new Boolean(is_test), project_id, 0);
    const wb = XLSX.utils.book_new();
    const sheet = XLSX.utils.aoa_to_sheet(data);
    sheet["!margins"] = merges;
    XLSX.utils.book_append_sheet(wb, sheet, name);
    const buf = XLSX.write(wb, { type: "buffer", bookType: "xlsx" });
    ctx.attachment(`${name}.xlsx`, {
        type: 'attachment'
    })
    ctx.body = buf;
}
const getProjectsExcelResult = async (mappers, ctx) => {
    const { where } = ctx.request.query;
    const wh = buildWhere(where);
    const project_id = wh.project_id;
    const is_test = wh.is_test;
    const ef_year = wh.ef_year;
    const wb = XLSX.utils.book_new();
    let filename = null;
    const projectMapper = new EvaProjectMapper;
    if (project_id) {
        const project = await projectMapper.findOne({ pk: project_id });
        filename = `${project.ef_year}年-${project.name}.xlsx`
        for (let i = 0; i < mappers.length; i++) {
            const { merges, data, name } = await mappers[i].exportXlsxSheet(new Boolean(is_test), project_id, 0, 0);
            const sheet = XLSX.utils.aoa_to_sheet(data);
            sheet["!merges"] = merges;
            sheet["!protect"] = {
                password: "tnnt"
            }
            XLSX.utils.book_append_sheet(wb, sheet, name);
        }

    } else if (ef_year) {

        const projects = await projectMapper.findList({ where: { ef_year } });
        filename = `${ef_year}年度特能集团领导干部测评结果汇总.xlsx`
        for (let i = 0; i < mappers.length; i++) {
            const sheet_merges = [];
            const sheet_data = [];
            let sheet_name = null;
            let rowStart = 0;
            let p_index = 0;
            for (let p = 0; p < projects.length; p++) {
                const { merges, data, name } = await mappers[i].exportXlsxSheet(new Boolean(is_test), projects[p].id, rowStart, p_index);
                for (const mg of merges) {
                    sheet_merges.push(mg);
                }
                for (const d of data) {
                    sheet_data.push(d);
                }
                if (data.length > 0) p_index++;
                if (!sheet_name) sheet_name = name;
                rowStart += data.length;
            }

            const sheet = XLSX.utils.aoa_to_sheet(sheet_data);
            sheet["!merges"] = sheet_merges;
            sheet["!protect"] = {
                password: "tnnt"
            }
            XLSX.utils.book_append_sheet(wb, sheet, sheet_name);
        }
    } else {
        ctx.body = "error parameters"
        return;
    }
    const buf = XLSX.write(wb, { type: "buffer", bookType: "xlsx" });
    ctx.attachment(filename, {
        type: 'attachment'
    })
    ctx.body = buf;
}

export { getExcelResult, getProjectsExcelResult }