import { BaseRouter } from "./base-router.js";
import { RuTopRuleMapper } from "../mapper/ru-top-rule.ts";
export class RuTopRuleRouter extends BaseRouter {
    constructor() {
        super(new RuTopRuleMapper())
    }
}