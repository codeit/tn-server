import Router from '@koa/router';
import { ResultDemoEvaMapper } from '../mapper/result-demo-eva.ts';
import { getExcelResult } from './result-base.js';
export class ResultDemoEvaRouter {
    router = new Router({ prefix: '/result-demo-eva' });
    resultMapper = new ResultDemoEvaMapper;
    getRouter() {
        this.router.get("/result", async (ctx) => {
            await getExcelResult(this.resultMapper, ctx);
        });
        return this.router;
    }
}