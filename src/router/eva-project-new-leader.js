import { BaseRouter } from "./base-router.js";
import { EvaProjectNewLeaderMapper } from "../mapper/eva-project-new-leader.ts";
export class EvaProjectNewLeaderRouter extends BaseRouter {
    constructor() {
        super(new EvaProjectNewLeaderMapper())
    }
}