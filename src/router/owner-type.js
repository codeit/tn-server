import { BaseRouter } from "./base-router.js";
import { OwnerTypeMapper } from "../mapper/owner-type.ts";
export class OwnerTypeRouter extends BaseRouter {
    constructor() {
        super(new OwnerTypeMapper())
    }
}