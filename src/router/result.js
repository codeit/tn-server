import Router from '@koa/router';
import { ResultDemoEvaMapper } from '../mapper/result-demo-eva.ts';
import { ResultExtDirectorMapper } from '../mapper/result-ext-director.ts';
import { ResultLeaderGroupMapper } from '../mapper/result-leader-group.ts';
import { ResultLeadersMapper } from '../mapper/result-leaders.ts';
import { ResultNewCadreMapper } from '../mapper/result-new-cadre.ts';
import { EvaProjectMapper } from '../mapper/eva-project.ts';
import { getProjectsExcelResult } from './result-base.js';
export class ResultRouter {
    router = new Router({ prefix: '/result' });
    resultMappers = [
        new ResultLeaderGroupMapper,
        new ResultLeadersMapper,
        new ResultDemoEvaMapper,
        new ResultNewCadreMapper,
        new ResultExtDirectorMapper
    ];
    projectMapper = new EvaProjectMapper;

    getRouter() {
        this.router.get("/index", async (ctx) => {
            const param = '?where=%7B%22project_id%22%3A%22e8c98db0-a19e-11ef-b97d-f0d4e2e766eb%22%7D'
            const paramy = '?where=%7B%22ef_year%22%3A2024%7D'
            const content = `
               <ol>
                 <li><a href='/result/project${param}' target='_blank'>但个总表 </a>  </li>
                 <li><a href='/result/project${paramy}' target='_blank'>汇总表 </a>  </li>
                 <li><a href='/result-leader-group/result${param}' target='_blank'>班子表 </a>  </li>
                 <li><a href='/result-leaders/result${param}' target='_blank'>领导人员表 </a>  </li>
                 <li><a href='/result-ext-director/result${param}' target='_blank'>外部董事 </a>  </li>
                 <li><a href='/result-demo-eva/result${param}' target='_blank'>民主评议 </a>  </li>
                 <li><a href='/result-new-cadre/result${param}' target='_blank'>新提拔干部 </a>  </li>
               </ol>
            `;
            ctx.body = 'index';

        })
        this.router.get("/project", async (ctx) => {
            await getProjectsExcelResult(this.resultMappers, ctx);
        })
        return this.router;
    }

}