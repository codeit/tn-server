import Router from '@koa/router';
import { ResultLeaderGroupMapper } from '../mapper/result-leader-group.ts';
import { getExcelResult } from './result-base.js';


export class ResultLeaderGroupRouter {
    router = new Router({ prefix: '/result-leader-group' });
    resultMapper = new ResultLeaderGroupMapper;

    getRouter() {
        this.router.get("/result", async (ctx) => {
            await getExcelResult(this.resultMapper, ctx);

        })

        return this.router;
    }
}