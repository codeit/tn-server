import { BaseRouter } from "./base-router.js";
import { EvaProjectMapper } from "../mapper/eva-project.ts";
export class EvaProjectRouter extends BaseRouter {
    constructor() {
        super(new EvaProjectMapper())
    }
}