import { BaseRouter } from "./base-router.js";
import { AdminUserMapper } from "../mapper/admin-user.js";
import bcrypt from 'bcrypt';
import { z } from 'zod'
import logger from "../lib/log4js.js";
import { buildWhere } from "../lib/database.js";
export class AdminUserRouter extends BaseRouter {
    constructor() {
        super(new AdminUserMapper())
    }

    getRouter() {
        const router = super.getRouter();
        router.post("/login", async (ctx) => {
            const { where } = ctx.request.body;
            const { username, password } = buildWhere(where);
            try {
                z.object({
                    username: z.string({ required_error: "用户名必须填写", }).trim(),
                    password: z.string({ required_error: "密码必须填写", })
                }).parse({ username, password });
            } catch (error) {
                logger.error(JSON.stringify(error));
                ctx.body = {
                    success: false,
                    code: 404,
                    message: error.issues.map(c => c.message).join(',')
                }
                return;
            }

            const userInfo = await this.mapper.findOneByQuery({ where: { name: username } })

            if (!userInfo) {
                ctx.body = {
                    success: false,
                    code: 404,
                    message: "用户不存在"
                }
                return;
            }
            const match = await bcrypt.compare(password, userInfo.store_pass);
            if (match) {
                userInfo['store_pass'] = "***"
                ctx.body = {
                    success: true,
                    code: 200,
                    data: userInfo,
                    message: "登录成功"
                }

                return
            } else {
                ctx.body = {
                    success: false,
                    code: 500,
                    data: false,
                    message: "登录失败,密码验证错误"
                }
            }
        })
        router.post("/change-password", async (ctx) => {
            const { where: { username, password, password2, npassword } } = ctx.request.body;
            const validArgs = z
                .object({
                    username: z.string({ required_error: "用户名必须填写", }),
                    password: z.string({ required_error: "旧密码必须填写", }),
                    password2: z.string({ required_error: "新密码必须填写", }),
                    npassword: z.string({ required_error: "密码确认必须填写", }),
                })
                .refine((data) => data.password2 === data.npassword, {
                    message: "两次密码不一致",
                    path: ["password2", "npassword"]
                })
                .safeParse({ username, password, password2, npassword });
            if (!validArgs.success) {
                ctx.body = {
                    code: 500,
                    success: false,
                    message: validArgs.error
                }
                return
            }
            const userInfo = await this.mapper.findOneByQuery({ where: { name: username } })
            if (!userInfo) {
                ctx.body = {
                    code: 300,
                    success: false,
                    message: "用户不存在"
                }
                return
            }
            const match = await bcrypt.compare(password, userInfo.store_pass);
            if (!match) {
                ctx.body = {
                    code: 300,
                    success: false,
                    message: "旧密码不正确"
                }
                return;
            }
            const hasedPass = await bcrypt.hash(npassword, 10);
            await this.mapper.update({ values: { store_pass: hasedPass }, where: { name: userInfo.name } });
            ctx.body = {
                code: 200,
                success: true,
                message: "密码修改成功"
            }

        })
        return router;
    }
}