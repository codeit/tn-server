import { BaseRouter } from "./base-router.js";
import { ObjectGroupMapper } from "../mapper/object-group.ts";
export class ObjectGroupRouter extends BaseRouter {
    constructor() {
        super(new ObjectGroupMapper())
    }
}