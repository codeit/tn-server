import { BaseRouter } from "./base-router.js";
import { RuSecondRuleMapper } from "../mapper/ru-second-rule.ts";
export class RuSecondRuleRouter extends BaseRouter {
    constructor() {
        super(new RuSecondRuleMapper())
    }

    getRouter() {
        const router = super.getRouter();
        router.get("/find-top-rule", async (ctx) => {
            const { id } = ctx.request.query;
            const topRule = await this.mapper.findTopRule(id);
            this.ctxBody(ctx, { data: topRule });
        })
        return router;
    }
}