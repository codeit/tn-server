import { BaseRouter } from "./base-router.js";
import { OwnerNameMapper } from "../mapper/owner-name.ts";
import logger from "../lib/log4js.js";
import { buildWhere } from "../lib/database.js";
export class OwnerNameRouter extends BaseRouter {
    constructor() {
        super(new OwnerNameMapper())
    }

    getRouter() {
        const router = super.getRouter();
        router.get("/find-project-owners", async (ctx) => {
            const { where } = ctx.request.query;
            const { project_id } = buildWhere(where);
            const results = await this.mapper.findOwnerNameByProject(project_id);
            ctx.body = { data: results };
        })


        return router;
    }
}