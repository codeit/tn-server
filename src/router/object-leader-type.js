import { BaseRouter } from "./base-router.js";
import { ObjectLeaderTypeMapper } from "../mapper/object-leader-type.ts";
export class ObjectLeaderTypeRouter extends BaseRouter {
    constructor() {
        super(new ObjectLeaderTypeMapper())
    }
}