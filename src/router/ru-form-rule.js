import { BaseRouter } from "./base-router.js";
import { RuFormRuleMapper } from "../mapper/ru-form-rule.ts";
import logger from "../lib/log4js.js";
export class RuFormRuleRouter extends BaseRouter {
    constructor() {
        super(new RuFormRuleMapper())
    }
    getRouter() {
        const router = super.getRouter();
        router.post("/check-weight", async (ctx) => {
            const { where } = ctx.request.body;
            const { form_id } = where;
            logger.debug(where);
            const result = await this.mapper.checkWeight(form_id);
            ctx.body = {
                success: result,
                code: result ? 200 : 500,
                message: result ? "权重正确" : "权重不等于100"
            }

        })
        return router;
    }
}