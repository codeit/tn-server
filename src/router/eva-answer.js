import Router from '@koa/router';
import { EvaAnswerMapper } from "../mapper/eva-answer.ts";
import logger from "../lib/log4js.js";
export class EvaAnswerRouter {
    constructor() {
        const prefix = "/eva-answer";
        this.prefix = prefix;
        this.router = new Router({ prefix });
        this.mapper = new EvaAnswerMapper;
    }
    getRouter() {
        this.router.post("/submit", async (ctx) => {
            const { answers } = ctx.request.body;
            if (answers.length === 0) {
                ctx.body = {
                    code: 500,
                    success: false,
                    message: '没有提交任何数据'
                }
                return
            }
            ctx.body = await this.mapper.submitAnswer(answers);
        })

        return this.router;
    }
}
