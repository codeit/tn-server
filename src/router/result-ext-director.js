import Router from '@koa/router';
import { ResultExtDirectorMapper } from '../mapper/result-ext-director.ts';
import { getExcelResult } from './result-base.js';

export class ResultExtDirectorRouter {
    router = new Router({ prefix: '/result-ext-director' });
    resultMapper = new ResultExtDirectorMapper;

    getRouter() {
        this.router.get("/result", async (ctx) => {
            await getExcelResult(this.resultMapper, ctx);
        })
        return this.router;
    }


}