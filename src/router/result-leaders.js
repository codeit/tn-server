import Router from '@koa/router';
import { ResultLeadersMapper } from '../mapper/result-leaders.ts';
import { getExcelResult } from './result-base.js';
export class ResultLeadersRouter {
    router = new Router({ prefix: '/result-leaders' });
    resultMapper = new ResultLeadersMapper;

    getRouter() {
        this.router.get("/result", async (ctx) => {
            await getExcelResult(this.resultMapper, ctx);
        })
        return this.router;
    }


}