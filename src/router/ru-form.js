import { BaseRouter } from "./base-router.js";
import { RuFormMapper } from "../mapper/ru-form.ts";
export class RuFormRouter extends BaseRouter {
    constructor() {
        super(new RuFormMapper())

    }
}