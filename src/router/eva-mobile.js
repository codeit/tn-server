import Router from '@koa/router';
import { EvaMobileMapper } from '../mapper/eva-mobile.js';
import { RuFormMapper } from '../mapper/ru-form.ts';
import { buildWhere } from '../lib/database.js';

class EvaMobileRouter {

    constructor() {
        this.mapper = new EvaMobileMapper;
        this.formMapper = new RuFormMapper;
        const prefix = "/eva-mobile/vote";
        this.router = new Router({ prefix });
    }

    getRouter() {
        this.router.get("/form-info", async (ctx) => {
            const { where } = ctx.request.query;
            const { project_id, project_owner_id } = buildWhere(where);
            const forms = await this.mapper.findVoteForms({ project_id, project_owner_id })
            if (!forms || forms.length === 0) {
                ctx.body = {
                    code: 500,
                    success: false,
                    data: [],
                    message: '当前用户无可用评测表',
                }
            }
            ctx.body = {
                code: 200,
                success: true,
                data: forms
            }

        })
        this.router.get("/form-users-rules", async (ctx) => {
            const { where } = ctx.request.query;
            const { project_id, project_owner_id, project_owner_form_id } = buildWhere(where);
            const form_info = await this.formMapper.findOne({ pk: project_owner_form_id });
            const users = await this.mapper.findProjectFormUser({ project_id, project_owner_id, project_owner_form_id });
            if (!users || users.length === 0) {
                ctx.body = {
                    success: false,
                    code: 500,
                    message: "无票数",
                    data: { form_info, users: [], rules: [] },
                }
                return;
            };
            const rules = await this.mapper.findFormRuleDetails({ form_id: project_owner_form_id });
            ctx.body = {
                success: true,
                code: 200,
                data: { form_info, users, rules }
            }
        })

        return this.router;
    }
}
export { EvaMobileRouter }