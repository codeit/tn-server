echo off
call setenv.bat
set ENV_NAME=.env.dev
if "%1"=="" goto runapp
set ENV_NAME=.env.%1
:runapp
deno run --env-file=%ENV_NAME%   --allow-all --watch-hmr  src\index.ts 