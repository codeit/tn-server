@echo off
call setenv.bat \eva
set ENV_NAME=.env
if "%1"=="" goto runapp
set ENV_NAME=.env.%1
:runapp
deno run --env-file=%ENV_NAME% --allow-all src\index.ts 